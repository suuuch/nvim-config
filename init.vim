"安装 vim-plug 这个插件本身，运行以下命令安装：
"curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
"    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
"
call plug#begin('/Users/shen/.local/share/nvim/site/autoload/plugged')



" Shorthand notation; fetches https://github.com/junegunn/vim-easy-align
Plug 'junegunn/vim-easy-align'

" Plugin outside ~/.vim/plugged with post-update hook
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }

" Airline Theme
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" NerdTree
Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeTabsToggle' }
Plug 'jistr/vim-nerdtree-tabs', { 'on': 'NERDTreeTabsToggle' }
Plug 'tpope/vim-fireplace', { 'for': 'python' }
Plug 'Xuyuanp/nerdtree-git-plugin'

" Auto Complete
" Use release branch (recommend)
Plug 'neoclide/coc.nvim', {'branch': 'release'}

" syntax checking and semantic errors
Plug 'dense-analysis/ale'

" Auto Pair
Plug 'jiangmiao/auto-pairs'
Plug 'zchee/deoplete-jedi'
Plug 'terryma/vim-multiple-cursors'

" Initialize plugin system
call plug#end()

"************行和列设置***************
" 显示行横线
set cursorline
" 显示行号
set nu

" NerdTree Configure
nnoremap <F3> :NERDTreeToggle<CR>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" *********** NERDTree插件配置 ***********
" 默认打开NERDTree
let NERDTreeChDirMode=2                                         " 设置当前目录为nerdtree的起始目录
let NERDChristmasTree=1                                         " 使得窗口有更好看的效果
let NERDTreeMouseMode=1                                         " 双击鼠标左键打开文件
let NERDTreeWinSize=25                                          " 设置窗口宽度为25
" 打开文件默认开启文件树
autocmd VimEnter * NERDTree

"忽略特定文件和目录
let NERDTreeIgnore=[ '\.pyc$', '\.pyo$', '\.py\$class$', '\.obj$',
            \ '\.o$', '\.so$', '\.egg$', '^\.git$', '__pycache__', '\.DS_Store' ]


" NerdTree Arrow
let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'

let g:NERDTreeGitStatusIndicatorMapCustom = {
                \ 'Modified'  :'✹',
                \ 'Staged'    :'✚',
                \ 'Untracked' :'✭',
                \ 'Renamed'   :'➜',
                \ 'Unmerged'  :'═',
                \ 'Deleted'   :'✖',
                \ 'Dirty'     :'✗',
                \ 'Ignored'   :'☒',
                \ 'Clean'     :'✔︎',
                \ 'Unknown'   :'?',
                \ }

" Buffer Change
set wildmenu wildmode=full 
set wildchar=<Tab> wildcharm=<C-Z>
"noremap <leader> :b <c-z>

" Airline Theme Configure
let g:airline#extensions#tabline#enabled = 1

" Deoplete
let g:deoplete#enable_at_startup = 1

autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif
inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"

" disable autocompletion, cause we use deoplete for completion
let g:jedi#completions_enabled = 0

" open the go-to function in split, not another buffer
let g:jedi#use_splits_not_buffers = "right"

let g:python3_host_prog = '/usr/bin/python3'



" ale Configure
" Check Python files with flake8 and pylint.
"
let b:ale_linters = ['flake8', 'pylint']
"
" " Fix Python files with autopep8 and yapf.
"
let b:ale_fixers = ['autopep8', 'yapf']
"
" " Disable warnings about trailing whitespace for Python files.
"
let b:ale_warn_about_trailing_whitespace = 0
